[![Agile611](https://www.agile611.com/wp-content/uploads/2020/09/cropped-logo-header.png)](http://www.agile611.com/)

# Práctica de Git

Vamos a conocer GIT para el control de versiones de nuestros ficheros - http://git-scm.com/

Sigue las instrucciones paso a paso con la ayuda del instructor.

## Requisitos previos: Entorno local

* Instalar Git - https://git-scm.com/
* Instalar Cliente Git - https://git-scm.com/downloads/guis

## Ejercicio

* Realizar los siguientes ejercicios - http://try.github.io

---

## Configuración general

```
git config --global user.name "name surname"
git config --global user.email email@example.com
git config --list
```

## Crear proyecto

```
mkdir temp
cd temp
git init
```

## Ver estado

```
git status
git log
git diff
.gitignore
```


## Descargar proyecto

```
git clone <URL.git>
```

## Commits

```
git add .
git commit -m "<message>"
```

## Compartir proyectos (colaborar)

```
git pull <remote_alias> <branch_name>
git remote add <remote_alias> <url>
(remote_alias == origin)
git remote -v
git push -u <remote_alias> <branch_name>
(remote_alias == origin)
git push -u origin --all
git push -u origin --tags
```

## Ramas

```
git branch <branch_name>
git checkout <branch_destination>
git merge <branch_origen>
git diff <branch_destination>..<branch_origen>
```

## Vocabulario básico

* HEAD: rama actual
* HEAD^: padre de HEAD
* HEAD-4: 4 pasos atras de HEAD

## Deshacer

```
* git checkout HEAD <file>
* git reset --hard HEAD
* git revert <commit>
* git reset --hard <commit>
* git reset <commit>
* git reset --keep <commit>
```

## Etiquetas

```
git tag -a v1.4 -m 'my version 1.4'
git tag
git show v1.4
git log --pretty=oneline
git tag -a v1.2 9fceb02
```

---
## Common networking problems

If you have proxies or VPNs running on your machine, it is possible that Vagrant is not able to provision your environment.

Please check your connectivity before.

## Support

This tutorial is released into the public domain by [Agile611](http://www.agile611.com/) under Creative Commons Attribution-NonCommercial 4.0 International.

[![License: CC BY-NC 4.0](https://img.shields.io/badge/License-CC_BY--NC_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nc/4.0/)


This README file was originally written by [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhs/) and is likewise released into the public domain.

Please contact Agile611 for further details.

* [Agile611](http://www.agile611.com/)
* Laureà Miró 309
* 08950 Esplugues de Llobregat (Barcelona)
